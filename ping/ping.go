// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package ping

import (
	"fmt"
	"net/http"
	"os"
	"spaziodati.eu/atoka-cli/common"
)

func Execute(args common.GlobalArgs) {
	var (
		client = http.Client{}
		url    = fmt.Sprintf("%s/v2/people?limit=0&token=%s", *args.BaseUrl, *args.Token)
	)

	rawBody, err := client.Get(url)

	if err != nil || rawBody.StatusCode != 200 {
		println("Ops, something went wrong :(")
		os.Exit(1)
	}

	println("Successfully pinged Atoka servers.\nEverything is fine.")
}
