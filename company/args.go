// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package company

import (
	"fmt"

	"gopkg.in/alecthomas/kingpin.v2"
	"spaziodati.eu/atoka-cli/common"
)

func SetUpUploadArgs(parentCmd *kingpin.CmdClause, globalArgs *common.GlobalArgs) (*kingpin.CmdClause, common.UploadArgs) {
	var (
		args      common.UploadArgs
		timestamp = common.GetTimeStamp()
	)

	companyUpload := parentCmd.Command("upload", "Upload portfolio of companies.")

	args.GlobalArgs = globalArgs

	args.Input = companyUpload.
		Arg("input", "Path of the input file with the atoka ids and data to upload. Please use a tab separated csv file.").
		Required().
		ExistingFile()

	args.Portfolio = companyUpload.
		Flag("customer-portfolio", "portfolio name").
		Short('c').
		Required().
		String()

	args.Report = companyUpload.
		Flag("report", "File where to write the report.").
		Short('r').
		Default(fmt.Sprintf("report_%s.json", timestamp)).
		PlaceHolder("report_<NOW>.json").
		String()

	args.AppendMode = companyUpload.
		Flag("append", "Append the data to the current version of the portfolio").
		Bool()

	return companyUpload, args
}
