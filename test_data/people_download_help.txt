usage: atoka people download [<flags>] <input>

Download people data.

Flags:
      --help             Show context-sensitive help (also try --help-long and
                         --help-man).
      --version          Show application version.
  -t, --token=TOKEN      API token to use for the HTTP requests.
  -s, --silent           Silent mode, don't print on the terminal.
  -p, --packages=* ...   Package to retrieve, can be specified multiple times
  -o, --output=output_<NOW>.json
                         File where to write the results.
  -r, --report=report_<NOW>.json
                         File where to write the report.
  -P, --pagesize=100000  Paginate output in files of PAGESIZE elements. Setting
                         it to 0 disables pagination

Args:
  <input>  Path of the input file with the tax codes.

