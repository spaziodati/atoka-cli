usage: atoka people upload --customer-portfolio=CUSTOMER-PORTFOLIO [<flags>] <input>

Upload portfolio of people.

Flags:
      --help         Show context-sensitive help (also try --help-long and
                     --help-man).
      --version      Show application version.
  -t, --token=TOKEN  API token to use for the HTTP requests.
  -s, --silent       Silent mode, don't print on the terminal.
  -c, --customer-portfolio=CUSTOMER-PORTFOLIO
                     portfolio name
  -r, --report=report_<NOW>.json
                     File where to write the report.
      --append       Append the data to the current version of the portfolio

Args:
  <input>  Path of the input file with the tax codes and data. Please use a tab
           separated csv file.

