usage: atoka ping

Check that everything works.

Flags:
      --help         Show context-sensitive help (also try --help-long and
                     --help-man).
      --version      Show application version.
  -t, --token=TOKEN  API token to use for the HTTP requests.
  -s, --silent       Silent mode, don't print on the terminal.

