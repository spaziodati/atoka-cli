usage: atoka people <command> [<args> ...]

Commands about people.

Flags:
      --help         Show context-sensitive help (also try --help-long and
                     --help-man).
      --version      Show application version.
  -t, --token=TOKEN  API token to use for the HTTP requests.
  -s, --silent       Silent mode, don't print on the terminal.

Commands:
  people
    download [<flags>] <input>
    upload --customer-portfolio=CUSTOMER-PORTFOLIO [<flags>] <input>
    status [<flags>]

