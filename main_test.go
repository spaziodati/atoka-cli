// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bytes"
	"io/ioutil"
	"regexp"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func fileContent(path string) string {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	return fixWhitespace(string(data))
}

func stripTrailingWhitespaces(input string) string {
	re := regexp.MustCompile("(?m)[\t ]+$")
	return re.ReplaceAllString(input, "")
}

func normalizeWhitespace(input string) string {
	re := regexp.MustCompile("(?m)[\t ]+")
	return re.ReplaceAllString(input, " ")
}

func fixWhitespace(input string) string{
	normalized := normalizeWhitespace(input)
	return stripTrailingWhitespaces(normalized)
}


var _ = Describe("CLI", func() {

	Context("Usage strings", func() {
		It("should print the right usage for the cli", func() {
			buffer := bytes.NewBuffer([]byte{})

			app.UsageWriter(buffer)
			app.Usage([]string{"--help"})
			usageString := fixWhitespace(buffer.String())

			Expect(usageString).Should(BeEquivalentTo(fileContent("test_data/plain_help.txt")))
		})

		It("should print the right usage for ping ", func() {
			buffer := bytes.NewBuffer([]byte{})

			app.UsageWriter(buffer)
			app.Usage([]string{"ping", "--help"})
			usageString := fixWhitespace(buffer.String())

			Expect(usageString).Should(BeEquivalentTo(fileContent("test_data/ping_help.txt")))
		})

		It("should print the right usage for the people command ", func() {
			buffer := bytes.NewBuffer([]byte{})

			app.UsageWriter(buffer)
			app.Usage([]string{"people", "--help"})
			usageString := fixWhitespace(buffer.String())

			Expect(usageString).Should(BeEquivalentTo(fileContent("test_data/people_help.txt")))
		})

		It("should print the right usage for people download ", func() {
			buffer := bytes.NewBuffer([]byte{})

			app.UsageWriter(buffer)
			app.Usage([]string{"people", "download", "--help"})
			usageString := fixWhitespace(buffer.String())

			Expect(usageString).Should(BeEquivalentTo(fileContent("test_data/people_download_help.txt")))
		})

		It("should print the right usage for people upload ", func() {
			buffer := bytes.NewBuffer([]byte{})

			app.UsageWriter(buffer)
			app.Usage([]string{"people", "upload", "--help"})
			usageString := fixWhitespace(buffer.String())

			Expect(usageString).Should(BeEquivalentTo(fileContent("test_data/people_upload_help.txt")))
		})
	})
})
