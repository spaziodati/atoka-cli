APP_NAME=atoka
BIN_DIR=bin
DIST_DIR=distrib
TARGETS=linux64 linux32 mac64 windows64 windows32
_BINS=$(addsuffix /$(APP_NAME),$(TARGETS))
BINS=$(patsubst windows%,windows%.exe,$(_BINS))
BIN_PATHS=$(addprefix $(BIN_DIR)/,$(BINS))
DIST_PATHS=$(addprefix $(DIST_DIR)/,$(addsuffix .zip,$(TARGETS)))

all: $(BIN_PATHS) | $(BIN_DIR)

$(BIN_DIR):
	mkdir -p $@

$(DIST_DIR):
	mkdir -p $@

$(filter-out windows%,$(TARGETS)): %: $(BIN_DIR)/%/$(APP_NAME)
$(filter windows%,$(TARGETS)): %: $(BIN_DIR)/%/$(APP_NAME).exe

$(BIN_DIR)/linux64/$(APP_NAME): GOENV=GOOS=linux GOARCH=amd64
$(BIN_DIR)/linux32/$(APP_NAME): GOENV=GOOS=linux GOARCH=386
$(BIN_DIR)/mac64/$(APP_NAME): GOENV=GOOS=darwin GOARCH=amd64
$(BIN_DIR)/windows64/$(APP_NAME).exe: GOENV=GOOS=windows GOARCH=amd64
$(BIN_DIR)/windows32/$(APP_NAME).exe: GOENV=GOOS=windows GOARCH=386

$(BIN_DIR)/%: main.go common/*.go company/*.go people/*.go ping/*.go
	$(GOENV) go build -o $@

dist: $(DIST_DIR) $(DIST_PATHS)

$(DIST_DIR)/%.zip: $(BIN_DIR)/%/$(APP_NAME).exe
	zip -r -j $@ $<

$(DIST_DIR)/%.zip: $(BIN_DIR)/%/$(APP_NAME)
	zip -r -j $@ $<

.PHONY: clean distclean

clean:
	@-rm -r $(BIN_DIR)/*

distclean:
	@-rm -r $(DIST_DIR)/*
