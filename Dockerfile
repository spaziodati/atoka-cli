FROM golang:1.9-stretch

WORKDIR /go/src/spaziodati.eu/atoka-cli
COPY . .

RUN apt-get update && apt-get -y install zip && apt-get clean
RUN go get github.com/onsi/ginkgo/ginkgo && \
    go install github.com/onsi/ginkgo/ginkgo
RUN go get github.com/onsi/gomega && \
    go install github.com/onsi/gomega

RUN go get github.com/golang/dep/cmd/dep && \
    go install github.com/golang/dep/cmd/dep && \
    dep ensure -vendor-only
