#!/bin/sh

set -ex

export BIN_DIR=bin
export DIST_DIR=distrib

make -e clean
make -e distclean

make -e dist
