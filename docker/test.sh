#!/bin/sh

set -x

ginkgo -r --randomizeAllSpecs --randomizeSuites --failOnPending --cover --trace --race --progress .