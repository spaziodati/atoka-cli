// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"io/ioutil"
	"os"

	"gopkg.in/alecthomas/kingpin.v2"
	"spaziodati.eu/atoka-cli/common"
	"spaziodati.eu/atoka-cli/company"
	"spaziodati.eu/atoka-cli/people"
	"spaziodati.eu/atoka-cli/ping"
)

var (
	clientVersion = "0.1.5"

	app = kingpin.
		New("atoka", "A command-line for atoka.").
		UsageTemplate(kingpin.CompactUsageTemplate).
		Version(clientVersion)

	globalArgs = common.SetupGlobalArgs(app, &clientVersion)

	cavalloCmd = app.Command("cavallo", "").Hidden()

	pingCmd = app.Command("ping", "Check that everything works.")

	peopleCmd                          = app.Command("people", "Commands about people.")
	peopleDownload, peopleDownloadArgs = people.SetUpDownloadArgs(peopleCmd, &globalArgs)
	peopleUpload, peopleUploadArgs     = people.SetUpUploadArgs(peopleCmd, &globalArgs)
	peopleStatus, peopleStatusArgs     = common.SetupStatusArgs(peopleCmd, &globalArgs)

	companyCmd                       = app.Command("company", "Commands about companies")
	companyUpload, companyUploadArgs = company.SetUpUploadArgs(companyCmd, &globalArgs)
	companyStatus, companyStatusArgs = common.SetupStatusArgs(companyCmd, &globalArgs)
)

func main() {
	switch kingpin.MustParse(app.Parse(os.Args[1:])) {

	case pingCmd.FullCommand():
		ping.Execute(globalArgs)

	case peopleDownload.FullCommand():
		people.ExecuteDownload(people.DefaultConfig(globalArgs), peopleDownloadArgs)

	case peopleUpload.FullCommand():
		people.ExecuteUpload(people.DefaultConfig(globalArgs), peopleUploadArgs)

	case peopleStatus.FullCommand():
		common.ExecuteStatus(common.DefaultConfig(globalArgs), peopleStatusArgs, "person")

	case companyUpload.FullCommand():
		company.ExecuteUpload(company.DefaultConfig(globalArgs), companyUploadArgs)

	case companyStatus.FullCommand():
		common.ExecuteStatus(common.DefaultConfig(globalArgs), companyStatusArgs, "company")

	case cavalloCmd.FullCommand():
		data, _ := ioutil.ReadFile("horse.txt")
		print(string(data))
	}
}
