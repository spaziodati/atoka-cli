// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	"fmt"
	"time"

	"gopkg.in/alecthomas/kingpin.v2"
)

type GlobalArgs struct {
	Token   *string
	Silent  *bool
	Version *string
	BaseUrl *string
}

type CommonArgs struct {
	GlobalArgs *GlobalArgs
	Input      *string
	Output     *string
	Report     *string
}

type DownloadArgs struct {
	CommonArgs
	Packages *[]string
	PageSize *int
}

type UploadArgs struct {
	CommonArgs
	Portfolio  *string
	AppendMode *bool
}

type StatusArgs struct {
	CommonArgs
	Portfolio *string
}

func SetupGlobalArgs(application *kingpin.Application, version *string) GlobalArgs {
	var args GlobalArgs

	args.Token = application.
		Flag("token", "API token to use for the HTTP requests.").
		Short('t').
		Envar("ATOKEN").
		Required().
		String()

	args.Silent = application.
		Flag("silent", "Silent mode, don't print on the terminal.").
		Short('s').
		Bool()

	args.Version = version

	args.BaseUrl = application.
		Flag("baseUrl", "for testing, can use a different api url").
		Envar("API_TOKEN").
		Default(baseUrl).
		Hidden().
		String()

	return args
}

func SetupStatusArgs(parentCmd *kingpin.CmdClause, globalArgs *GlobalArgs) (*kingpin.CmdClause, StatusArgs) {
	var (
		args StatusArgs
	)
	statusCmd := parentCmd.Command("status", "Get status info for a portfolio. (QUEUED, MATCHING, MATCHING_DONE, WRITING, SYNC)")

	args.GlobalArgs = globalArgs

	args.Portfolio = statusCmd.
		Flag("customer-portfolio", "optional portfolio name (if none is given all portfolio are returned)").
		Short('c').
		String()

	return statusCmd, args
}

func GetTimeStamp() string {
	t := time.Now()
	return fmt.Sprintf(
		"%d%.2d%.2d_%.2d%.2d%.2d",
		t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second(),
	)
}
