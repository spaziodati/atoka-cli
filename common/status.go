// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	"fmt"
	"os"
	"time"
)

type ProgressResponseItem struct {
	Collection string     `json:"collection"`
	Status     string     `json:"status"`
	Type       string     `json:"type"`       // person / company
	UploadType string     `json:"uploadType"` // perimeter / custom data
	TotalItems int        `json:"totalItems"` // (total number of records)
	Lag        int        `json:"lag"`        // (how many records do i have left to match/sync)
	Started    *time.Time `json:"started,omitempty"`
	Ended      *time.Time `json:"ended,omitempty"`
}

func (item *ProgressResponseItem) getInfo() string {
	msg := fmt.Sprintf("\tStatus:     %32s\n", item.Status)

	if item.TotalItems > 0{
		msg += fmt.Sprintf("\tTotal:      %32d\n", item.TotalItems)
	}

	if item.Lag > 0 {
		msg += fmt.Sprintf("\tLag:        %32d\n", item.Lag)
	}

	if item.Started != nil {
		msg += fmt.Sprintf("\tStarted at: %32s\n", item.Started.Format("15:04:05 2 Jan 2006"))
	}
	if item.Ended != nil {
		msg += fmt.Sprintf("\tEnded at:   %32s\n", item.Ended.Format("15:04:05 2 Jan 2006"))
	}
	return msg
}

type ProgressSchemaResponse struct {
	Items []ProgressResponseItem `json:"items"`
}

type ProgressResponse struct {
	Items map[string][]ProgressResponseItem `json:"items"`
}

func ExecuteStatus(conf AAConf, args StatusArgs, collectionType string) {
	var (
		response ProgressResponse
		err      error
	)

	if !*args.GlobalArgs.Silent {
		println("Client version:", *args.GlobalArgs.Version)
		println("Using API token:", *args.GlobalArgs.Token)
		println()
	}

	client := NewAtokaApiClient(conf, "", "", "", func(item AAItem) string { return "" })

	if args.Portfolio != nil && *args.Portfolio != "" {
		response, err = client.GetStatusForCollection(args.GlobalArgs, *args.Portfolio, collectionType)
	} else {
		response, err = client.GetStatusForAll(args.GlobalArgs, collectionType)
	}
	if err != nil {
		println(err.Error())
		os.Exit(1)
	}

	for name, items := range response.Items {

		if len(items) > 0 {

			fmt.Printf("Collection: %s\n", name)

			first := items[0].getInfo()

			fmt.Println(first)
		}

	}
}
