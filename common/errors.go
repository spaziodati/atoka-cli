// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	"errors"
	"net/http"
)

type APIError struct {
	error
	Fatal bool
}

func NewApiError(statusCode int, fatal bool) (aErr *APIError) {
	aErr = new(APIError)
	aErr.error = errors.New(http.StatusText(statusCode))
	aErr.Fatal = fatal

	if statusCode == http.StatusUnauthorized {
		aErr.error = errors.New("The token you're using is not authorized to use this command")
	}

	return
}

func NewCustomDataError(statusCode int) (aErr *APIError) {
	aErr = NewApiError(statusCode, true)

	if statusCode == http.StatusNotFound {
		aErr.error = errors.New("The specified portfolio does not exists")

	}
	return
}
