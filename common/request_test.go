// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/ghttp"
	"net/http"
)

var _ = Describe("Request", func() {

	Describe("handling responses", func() {
		var (
			server         *ghttp.Server
			atokaApiClient *AtokaApiClient
		)
		BeforeEach(func() {
			server = ghttp.NewServer()
			client := NewAtokaApiClient(
				DefaultConfig(GlobalArgs{}),
				"/v2/test_data",
				CustomDataUploadEndpoint,
				"person",
				func(item AAItem) string {
					return item["base"].(map[string]interface{})["taxId"].(string)
				},
			)
			atokaApiClient = &client
		})

		Context("when API returns an error code and HTML", func() {

			It("should return 504 errors instead of unmarshal error", func() {
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("GET", "/"),
						ghttp.RespondWithJSONEncoded(http.StatusGatewayTimeout, "<html>stuff</html>"),
					),
				)
				response, err := http.Get(server.URL())

				_, apiError := atokaApiClient.handleApiResponse(response, err)

				Ω(apiError.Error()).Should(Equal("Gateway Timeout"))
			})

			It("should return 502 errors instead of unmarshal error", func() {
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("GET", "/"),
						ghttp.RespondWithJSONEncoded(http.StatusBadGateway, "<html>stuff</html>"),
					),
				)
				response, err := http.Get(server.URL())

				_, apiError := atokaApiClient.handleApiResponse(response, err)

				Ω(apiError.Error()).Should(Equal("Bad Gateway"))
			})
		})
	})
})
