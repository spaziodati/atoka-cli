// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	"context"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"golang.org/x/sync/errgroup"
	"os"
)

var _ = Describe("Input Txt", func() {
	var (
		ctx                context.Context
		errGroup           *errgroup.Group
		transformId        func(string) string
		items              []*CustomDataInputItem
		inputFilePath      string
		countCallTransform int
		doInput            func()
	)

	GetOutput := func(channel <-chan *CustomDataInputItem) []*CustomDataInputItem {
		items := make([]*CustomDataInputItem, 0)
		for item := range channel {
			items = append(items, item)
		}

		Eventually(channel).Should(BeClosed())
		return items
	}

	BeforeEach(func() {
		ctx = context.Background()
		errGroup, ctx = errgroup.WithContext(ctx)
		countCallTransform = 0
		transformId = func(id string) string {
			countCallTransform = countCallTransform + 1
			return id
		}
	})

	doInput = func() {
		fullPath := GetTestFilePath(inputFilePath)
		ip := &InputProcessor{
			ValidateId:     func(_ string) bool { return true },
			MatchFieldName: "ID",
			TransformId:    transformId,
			Silent:         true,
			InputFile:      fullPath,
		}
		channel := ip.ReadTxtInput(ctx, errGroup)
		items = GetOutput(channel)
	}

	JustBeforeEach(doInput)

	CheckPerimetro := func() {
		for _, item := range items {
			Expect(item).To(Not(BeNil()))
			Expect(item.Key).To(HavePrefix("IAI"))
			Expect(item.Data).To(HaveLen(1))
			Expect(item.Data).To(HaveKey("ID"))
			Expect(item.Data["ID"]).To(Equal(item.Key))
		}
	}

	CheckLen := func() {
		Expect(items).To(HaveLen(5))
		Expect(countCallTransform).To(Equal(5))
	}

	Context("Input one column no header", func() {
		BeforeEach(func() { inputFilePath = "input_1_col_no_header.txt" })
		It("should call transform on all lines", CheckLen)
		It("should import as perimetro", CheckPerimetro)
	})

	Context("Input one column with header", func() {
		BeforeEach(func() { inputFilePath = "input_1_col_header.txt" })
		It("should call transform on 1-n lines", CheckLen)
		It("should import as perimetro", CheckPerimetro)
	})

	Context("Input has more than one column, no header", func() {
		BeforeEach(func() { inputFilePath = "input_2_col_no_header.txt" })
		It("Should return no items", func() {
			Expect(items).To(HaveLen(0))
		})
		It("Should return error", func() {
			Expect(errGroup.Wait()).Should(
				MatchError(MatchRegexp("Invalid file: we could not find a match field .* in the header (.*)")))
		})
	})

	Context("Input has more than one column, with header", func() {
		BeforeEach(func() { inputFilePath = "input_3_col_header.txt" })

		It("should call transform on lines 1-n", CheckLen)

		It("should populate item data with columns values", func() {
			for _, item := range items {
				Expect(item).To(Not(BeNil()))
				Expect(item.Key).To(HavePrefix("IAI"))
				Expect(item.Data).To(HaveKey("ID"))
				Expect(item.Data).To(HaveKey("my_col"))
				Expect(item.Data["ID"]).To(Equal(item.Key))
				Expect(item.Data["my_col"]).To(HavePrefix("data"))
			}
		})

	})

	Context("Input has more than one column, with header", func() {
		BeforeEach(func() { inputFilePath = "input_2_missing_key.txt" })

		It("should call transform on lines 1-n", CheckLen)

		It("should populated item data with columns values", func() {
			for _, item := range items {
				Expect(item).To(Not(BeNil()))
				Expect(item.Key).To(HavePrefix("IAI"))
				Expect(item.Data).To(HaveKey("ID"))
				Expect(item.Data).To(HaveKey("my_col"))
				Expect(item.Data["ID"]).To(Equal(item.Key))
				Expect(item.Data["my_col"]).To(HavePrefix("data"))
			}
		})

	})

	Context("CustomDataItemInput", func(){
		BeforeEach(func() { inputFilePath = "input_3_col_header.txt" })
		It("should match the size of the generated temp file", func(){
			var(dataSize, headerSize int)

			for _, item := range items{
				itemHeader := item.headerSize()

				dataSize += item.sizeInBytes()
				if headerSize < itemHeader{
					headerSize = itemHeader
				}
			}

			tmpPath, err := PrepareFile(ctx, errGroup, items, true)
			if err != nil{
				panic(err)
			}

			tmpFile, err := os.Open(tmpPath)
			if err != nil{
				panic(err)
			}

			fileInfo, err :=tmpFile.Stat()
			if err != nil{
				panic(err)
			}

			tmpSize := int(fileInfo.Size())

			Expect(dataSize + headerSize).To(Equal(tmpSize))
		})
	})

	Context("ChunkMaxBytes", func(){
		BeforeEach(func() { inputFilePath = "input_3_col_header.txt" })
		It("should correctly chunk the input", func(){
			const MaxBytes = 128 // must be greater than the largest custom data row and smaller than 1 MB

			fullPath := GetTestFilePath(inputFilePath)
			ip := &InputProcessor{
				ValidateId:     func(_ string) bool { return true },
				MatchFieldName: "ID",
				TransformId:    transformId,
				Silent:         true,
				InputFile:      fullPath,
			}
			channel := ip.ReadTxtInput(ctx, errGroup)

			chunked := ChunkOfMaxBytes(ctx, errGroup, channel, MaxBytes)

			for chunk := range chunked{
				tmpPath, err := PrepareFile(ctx, errGroup, chunk, true)
				if err != nil{
					panic(err)
				}

				tmpFile, err := os.Open(tmpPath)
				if err != nil{
					panic(err)
				}

				fileInfo, err :=tmpFile.Stat()
				if err != nil{
					panic(err)
				}

				tmpSize := int(fileInfo.Size())
				Expect(tmpSize < MaxBytes).Should(BeTrue())
			}
		})
	})
})
