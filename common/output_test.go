// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	"context"
	"io/ioutil"
	"os"

	"fmt"

	"encoding/json"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"golang.org/x/sync/errgroup"
)

func parseResponse(path string) AAResponse {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var response AAResponse
	err = json.Unmarshal(data, &response)
	if err != nil {
		panic(err)
	}
	return response
}

var _ = Describe("Output", func() {
	var (
		ctx context.Context
		g   *errgroup.Group
	)
	BeforeEach(func() {
		ctx = context.Background()
		g, ctx = errgroup.WithContext(ctx)
	})

	Describe("Writing Report", func() {
		var (
			reportSink chan ReportEntry
			reportPath *os.File
			err        error
		)

		BeforeEach(func() {
			reportSink = make(chan ReportEntry, 100)
			reportPath, err = ioutil.TempFile("", "")
			if err != nil {
				panic(err)
			}
		})

		AfterEach(func() {
			os.Remove(reportPath.Name())
		})

		It("Should write the given reportEntry", func() {
			WriteDownloadReport(ctx, g, reportSink, reportPath.Name(), false)

			reportSink <- ReportEntry{Key: "1", Status: ReportNotFound}
			reportSink <- ReportEntry{Key: "2", Status: ReportNotFound}
			reportSink <- ReportEntry{Key: "1", Status: ReportFound}
			reportSink <- ReportEntry{Key: "2", Status: ReportDuplicate}
			reportSink <- ReportEntry{Key: "3", Status: ReportInvalid}
			reportSink <- ReportEntry{Key: "4", Status: ReportNotFound}
			close(reportSink)

			Eventually(func() []byte {
				str, _ := ioutil.ReadFile(reportPath.Name())
				return str
			}).Should(MatchJSON(`{
                    "items": {
                    "1": "found",
                    "2": "not_found",
                    "3": "invalid",
                    "4": "not_found"},
                    "meta": {
              "duplicates": 1,
              "found": 1,
              "found_multiple": 0,
              "invalid": 1,
              "not_found": 2,
              "input_size": 5,
              "output_size": 1
            }}`))
		})

		It("Should still write the report if the user passes a list of invalid taxIds", func() {
			WriteDownloadReport(ctx, g, reportSink, reportPath.Name(), false)

			reportSink <- ReportEntry{Key: "1", Status: ReportInvalid}
			reportSink <- ReportEntry{Key: "2", Status: ReportInvalid}
			reportSink <- ReportEntry{Key: "3", Status: ReportInvalid}
			reportSink <- ReportEntry{Key: "4", Status: ReportInvalid}
			reportSink <- ReportEntry{Key: "5", Status: ReportInvalid}
			close(reportSink)

			Eventually(func() []byte {
				str, _ := ioutil.ReadFile(reportPath.Name())
				return str
			}).Should(MatchJSON(`{
                    "items": {
                    "1": "invalid",
                    "2": "invalid",
                    "3": "invalid",
                    "4": "invalid",
                    "5": "invalid"
            },
                    "meta": {
              "duplicates": 0,
              "found": 0,
              "found_multiple": 0,
              "invalid": 5,
              "not_found": 0,
              "input_size": 5,
              "output_size": 0
            }}`))
		})

		It("Should still write the report if the user passes a list of duplicated taxIds", func() {
			WriteDownloadReport(ctx, g, reportSink, reportPath.Name(), false)

			reportSink <- ReportEntry{Key: "1", Status: ReportNotFound}
			reportSink <- ReportEntry{Key: "1", Status: ReportDuplicate}
			reportSink <- ReportEntry{Key: "1", Status: ReportDuplicate}
			reportSink <- ReportEntry{Key: "1", Status: ReportFound}
			reportSink <- ReportEntry{Key: "1", Status: ReportDuplicate}
			reportSink <- ReportEntry{Key: "1", Status: ReportDuplicate}
			close(reportSink)

			Eventually(func() []byte {
				str, _ := ioutil.ReadFile(reportPath.Name())
				return str
			}).Should(MatchJSON(`{
                    "items": {
                    "1": "found"
            },
                    "meta": {
              "duplicates": 4,
              "found": 1,
              "found_multiple": 0,
              "invalid": 0,
              "not_found": 0,
              "input_size": 5,
              "output_size": 1
            }}`))
		})

	})

	Describe("Writing output", func() {
		var (
			outputSink chan AAItem
			outputPath *os.File
			err        error
		)

		BeforeEach(func() {
			outputSink = make(chan AAItem, 100)
			outputPath, err = ioutil.TempFile("", "")
			if err != nil {
				panic(err)
			}
		})

		It("Should write in output the given items", func() {
			WriteOutput(ctx, g, outputSink, outputPath.Name(), 0, false)

			aaitems := parseResponse(GetTestFilePath("response_small.json")).Items
			for _, item := range aaitems {
				outputSink <- item
			}
			close(outputSink)

			str, _ := ioutil.ReadFile(GetTestFilePath("output_small.json"))

			Eventually(func() string {
				str, _ := ioutil.ReadFile(outputPath.Name())
				return string(str)
			}).Should(MatchJSON(str))
		})

		It("Should write in output the given items (bigger)", func() {
			WriteOutput(ctx, g, outputSink, outputPath.Name(), 0, false)

			aaitems := parseResponse(GetTestFilePath("response_normal.json")).Items
			for _, item := range aaitems {
				outputSink <- item
			}
			close(outputSink)

			str, _ := ioutil.ReadFile(GetTestFilePath("output_normal.json"))

			Eventually(func() string {
				str, _ := ioutil.ReadFile(outputPath.Name())
				return string(str)
			}).Should(MatchJSON(str))
		})
		It("Should paginate correctly the output", func() {
			WriteOutput(ctx, g, outputSink, outputPath.Name(), 20, false)

			aaitems := parseResponse(GetTestFilePath("response_normal.json")).Items
			for _, item := range aaitems {
				outputSink <- item
			}
			close(outputSink)

			str, _ := ioutil.ReadFile(GetTestFilePath("output_normal.0.json"))

			Eventually(func() string {
				str, _ := ioutil.ReadFile(fmt.Sprintf("%s.0", outputPath.Name()))
				return string(str)
			}).Should(MatchJSON(str))

			str, _ = ioutil.ReadFile(GetTestFilePath("output_normal.1.json"))

			Eventually(func() string {
				str, _ := ioutil.ReadFile(fmt.Sprintf("%s.1", outputPath.Name()))
				return string(str)
			}).Should(MatchJSON(str))

			str, _ = ioutil.ReadFile(GetTestFilePath("output_normal.2.json"))

			Eventually(func() string {
				str, _ := ioutil.ReadFile(fmt.Sprintf("%s.2", outputPath.Name()))
				return string(str)
			}).Should(MatchJSON(str))

		})

	})

	Describe("bytesToHuman", func() {
		It("Should handle bytes", func() {
			Expect(bytesToHuman(10)).Should(Equal("10 bytes"))
			Expect(bytesToHuman(123)).Should(Equal("123 bytes"))
		})
		It("Should handle kilobytes", func() {
			Expect(bytesToHuman(1000)).Should(Equal("1.00 kb"))
			Expect(bytesToHuman(12354)).Should(Equal("12.35 kb"))
			Expect(bytesToHuman(42343)).Should(Equal("42.34 kb"))
		})
		It("Should handle megabytes", func() {
			Expect(bytesToHuman(1000000)).Should(Equal("1.00 mb"))
			Expect(bytesToHuman(12354000)).Should(Equal("12.35 mb"))
			Expect(bytesToHuman(423430)).Should(Equal("0.42 mb"))
		})
		It("Should handle gigabytes", func() {
			Expect(bytesToHuman(1000000000)).Should(Equal("1.00 gb"))
			Expect(bytesToHuman(12354000000)).Should(Equal("12.35 gb"))
			Expect(bytesToHuman(423430000)).Should(Equal("0.42 gb"))
		})
	})

})
