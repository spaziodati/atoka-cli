// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"github.com/tealeg/xlsx"
	"golang.org/x/sync/errgroup"
	"gopkg.in/cheggaaa/pb.v1"
)

const (
	CSV_SEPARATOR  = ','
	KEY_FIELD_NAME = "key"
)

type InputProcessor struct {
	ValidateId          func(string) bool
	MatchDataId         func(string) bool
	TransformId         func(string) string
	Silent              bool
	InputFile           string
	MatchFieldName      string
	DefaultReportStatus string
}

func ChunkOfMaxBytes(ctx context.Context, g *errgroup.Group, input <-chan *CustomDataInputItem, maxBytes int) <-chan []*CustomDataInputItem {
	output := make(chan []*CustomDataInputItem)

	g.Go(func() error {
		defer close(output)

		if maxBytes <= 0 {
			panic("ChunkOfMaxBytes must use a chunksize greater than 0")
		}

		accum := make([]*CustomDataInputItem, 0, maxBytes / 1000)
		accumByteSize := 0
		headerByteSize := 0

		for item := range input {
			itemSize := item.sizeInBytes()
			itemHeader := item.headerSize()

			if headerByteSize < itemHeader{
				headerByteSize = itemHeader
			}

			if accumByteSize + itemSize + headerByteSize > maxBytes {
				select {
				case output <- accum:
				case <-ctx.Done():
					return ctx.Err()
				}
				accum = make([]*CustomDataInputItem, 0, maxBytes / 1000) // new slice to avoid data races
				accumByteSize = 0
			}

			accum = append(accum, item)
			accumByteSize += itemSize
		}
		if len(accum) > 0 {
			select {
			case output <- accum:
			case <-ctx.Done():
				return ctx.Err()
			}
		}
		return nil
	})

	return output
}

func getRowColsXlsx(row *xlsx.Row) []string {
	cols := make([]string, len(row.Cells))
	for i, cell := range row.Cells {
		cols[i] = cell.String()
	}
	return cols
}

func getHeader(fields []string, matchFieldName string) (header []string, skip bool, err error) {
	skip = false

	if len(fields) == 1 {
		// it's a perimeter
		// check if it has a header
		if fields[0] == matchFieldName {
			log.Println("Found header with one column, uploading as perimeter")
			header = fields
			skip = true
			return
		} else {
			log.Println("No header line found, uploading as perimeter")
			header = []string{matchFieldName}
			skip = false
		}
		return
	}

	// trying to import something, we need an ID to match data with Atoka
	for _, value := range fields {
		if value == matchFieldName {
			// if the match field name is in this row, this is the header
			log.Printf(
				"Found header with match field %q and additional data, uploading as custom data\n",
				matchFieldName)
			header = fields
			skip = true
			return
		}
	}

	err = errors.New(fmt.Sprintf(
		"Invalid file: we could not find a match field %q in the header (%s)",
		matchFieldName, strings.Join(fields, fmt.Sprintf("%c", CSV_SEPARATOR))))
	return
}

func parseCustomDataRow(header []string, fields []string, matchFieldName string, transformId func(string) string) (item *CustomDataInputItem, err error) {
	item = &CustomDataInputItem{
		Key:  "",
		Data: make(map[string]string),
	}

	if len(header) != len(fields) {
		err = errors.New(fmt.Sprint(
			"Parsing row:", strings.Join(fields, fmt.Sprintf("%c", CSV_SEPARATOR)),
			", wrong number of columns. Make sure your file is a tab separated csv, and has an header row if you are importing custom data"))
		return
	}
	for i, col := range fields {
		colHeader := header[i]
		if i == 0 && colHeader == KEY_FIELD_NAME {
			item.Key = transformId(col)
			continue
		}
		if item.Key == "" && colHeader == matchFieldName {
			item.Key = transformId(col)
		}

		item.Data[colHeader] = col
	}

	if item.Key == "" {
		err = errors.New("didn't find key or match field in row")
	}

	return
}

func (ip *InputProcessor) ReadTxtInput(ctx context.Context, g *errgroup.Group) <-chan *CustomDataInputItem {
	outputChan := make(chan *CustomDataInputItem)

	g.Go(func() error {
		defer close(outputChan)

		realReader, err := os.Open(ip.InputFile)
		if err != nil {
			return err
		}
		defer realReader.Close()

		info, err := realReader.Stat()
		if err != nil {
			return err
		}
		progressBar := pb.New64(info.Size()).SetUnits(pb.U_BYTES)
		if !ip.Silent {
			progressBar.Start()
		}

		reader := progressBar.NewProxyReader(realReader)
		defer reader.Close()

		var header []string
		csvReader := csv.NewReader(reader)
		csvReader.Comma = CSV_SEPARATOR
		for {
			row, err := csvReader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatal(err)
			}
			skip := false

			if header == nil {
				header, skip, err = getHeader(row, ip.MatchFieldName)
				if err != nil {
					return err
				}
			}

			if !skip {
				data, err := parseCustomDataRow(header, row, ip.MatchFieldName, ip.TransformId)
				if err != nil {
					log.Println(err)
				} else {
					select {
					case outputChan <- data:
					case <-ctx.Done():
						return ctx.Err()
					}
				}
			}
		}
		if !ip.Silent {
			progressBar.Finish()
			println()
		}
		return nil
	})

	return outputChan
}

func (ip *InputProcessor) ReadXlsxInput(ctx context.Context, g *errgroup.Group) <-chan *CustomDataInputItem {

	outputChan := make(chan *CustomDataInputItem)

	g.Go(func() error {
		defer close(outputChan)

		statInfo, err := os.Stat(ip.InputFile)
		if err != nil {
			return err
		}

		bigFile := statInfo.Size() > 1000000

		if !ip.Silent && bigFile {
			println("Opening", ip.InputFile, " (this could take some time) ...")
		}
		xlFile, err := xlsx.OpenFile(ip.InputFile)
		if err != nil {
			return errors.New(fmt.Sprintf("ERROR %s is not a valid xlsx file.", ip.InputFile))
		}

		totalCells := 0
		if !ip.Silent {
			for _, sheet := range xlFile.Sheets {
				for _, row := range sheet.Rows {
					for range row.Cells {
						totalCells++
					}
				}
			}
		}

		progressBar := pb.New(totalCells)
		if !ip.Silent {
			println("Processing cells")
			progressBar.Start()
		}

		var header []string

		for _, sheet := range xlFile.Sheets {

			for _, row := range sheet.Rows {
				skip := false
				cols := getRowColsXlsx(row)

				if header == nil {
					header, skip, err = getHeader(cols, ip.MatchFieldName)
					if err != nil {
						return err
					}
				}
				if !skip {
					data, err := parseCustomDataRow(header, cols, ip.MatchFieldName, ip.TransformId)
					if err != nil {
						log.Println(err)
					} else {
						match := ip.MatchDataId(data.Key)
						if match {
							select {
							case outputChan <- data:
							case <-ctx.Done():
								return ctx.Err()
							}
						}
					}
					progressBar.Increment()
				}
			}
		}

		if !ip.Silent {
			progressBar.Finish()
			println()
		}

		return nil
	})

	return outputChan
}

func (ip *InputProcessor) ProcessInput(ctx context.Context, g *errgroup.Group, reportChan chan<- ReportEntry) <-chan *CustomDataInputItem {
	var (
		inputReader <-chan *CustomDataInputItem
		inputItems  = make(chan *CustomDataInputItem)
		inputMap    = make(map[string]bool)
	)

	if strings.HasSuffix(ip.InputFile, ".xlsx") {
		inputReader = ip.ReadXlsxInput(ctx, g)
	} else {
		inputReader = ip.ReadTxtInput(ctx, g)
	}

	g.Go(func() error {
		defer close(inputItems)

		header := true
		for item := range inputReader {
			status := ip.DefaultReportStatus
			itemKey := item.Key

			// duplicate?
			if _, ok := inputMap[itemKey]; ok {
				status = ReportDuplicate
			} else {
				if header && item.Key == ip.MatchFieldName {
					header = false
				} else {
					// validate taxId
					if !ip.ValidateId(item.Key) {
						status = ReportInvalid
					}
				}
			}

			inputMap[itemKey] = true
			entry := ReportEntry{itemKey, status}

			select {
			case reportChan <- entry:
			case <-ctx.Done():
				return ctx.Err()
			}

			if status == ip.DefaultReportStatus {
				select {
				case inputItems <- item:
				case <-ctx.Done():
					return ctx.Err()
				}
			}
		}

		return nil
	})
	return inputItems
}
