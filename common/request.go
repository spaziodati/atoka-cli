// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/sync/errgroup"
)

type AtokaApiClient struct {
	*http.Client
	downloadEndpoint string
	uploadEndpoint   string
	extractKey       func(item AAItem) string
	docType          string
	conf             AAConf
}

func NewAtokaApiClient(conf AAConf, downloadEndpoint string, uploadEndpoint string, docType string, extractKey func(item AAItem) string) AtokaApiClient {
	client := &http.Client{
		Timeout: conf.HttpDefaultTimeout,
	}

	return AtokaApiClient{
		client,
		fmt.Sprintf("%s%s", conf.BaseUrl, downloadEndpoint),
		fmt.Sprintf("%s%s", conf.BaseUrl, uploadEndpoint),
		extractKey,
		docType,
		conf,
	}
}

func (aac *AtokaApiClient) performRequest(endpoint string, params url.Values) (AAResponse, error) {
	rawResponse, err := aac.PostForm(endpoint, params)
	return aac.handleApiResponse(rawResponse, err)
}

func (aac *AtokaApiClient) performUploadRequest(body bytes.Buffer, pathParam string, params url.Values, contentType string) (AAResponse, error) {
	endpoint := fmt.Sprintf("%s/%s?%s", aac.uploadEndpoint, pathParam, params.Encode())

	// Now that you have a form, you can submit it to your handler.
	req, err := http.NewRequest("POST", endpoint, &body)
	if err != nil {
		return AAResponse{Error: true, Message: err.Error()}, NewApiError(http.StatusBadRequest, true)
	}
	// Don't forget to set the content type, this will contain the boundary.
	req.Header.Set("Content-Type", contentType)

	return aac.handleApiResponse(aac.Do(req))
}

func (aac *AtokaApiClient) handleApiResponse(rawResponse *http.Response, err error) (AAResponse, error) {
	var (
		responseBody []byte
		response     AAResponse
	)

	if err != nil {
		return AAResponse{Error: true, Message: "Can't reach the server. Check your network and/or your firewall.  You can check the status of our servers here (http://status.spaziodati.eu/2129311)"}, NewApiError(http.StatusBadRequest, false)
	}
	defer rawResponse.Body.Close()

	responseBody, err = ioutil.ReadAll(rawResponse.Body)
	if err != nil {
		return AAResponse{Error: true, Message: "ReadBodyError"}, NewApiError(http.StatusBadRequest, false)
	}

	switch rawResponse.StatusCode {
	case http.StatusUnauthorized, http.StatusForbidden, http.StatusBadRequest:
		return response, NewApiError(rawResponse.StatusCode, true)
	case http.StatusBadGateway, http.StatusGatewayTimeout, http.StatusTooManyRequests:
		return response, NewApiError(rawResponse.StatusCode, false)
	}

	err = json.Unmarshal(responseBody, &response)
	if err != nil {
		return AAResponse{Error: true, Message: "UnmarshalError"}, NewApiError(http.StatusBadRequest, false)
	}

	if response.Error {
		return response, errors.New(response.Message)
	}

	return response, nil
}

// requestWorker takes groups of maxLimit tax codes from input and puts AAItems in output
func (aac *AtokaApiClient) requestWorker(ctx context.Context, wg *sync.WaitGroup, input <-chan string, params url.Values, output chan<- AAItem, reportChan chan<- ReportEntry) error {
	defer wg.Done()

	// This is necessary to prevent a data race between workers
	requestParams, err := url.ParseQuery(params.Encode())
	if err != nil {
		return err
	}

	pageSize := maxLimit
	requestParams.Set("limit", strconv.Itoa(pageSize))

	for ids := range input {
		requestParams.Set("taxIds", ids)

		pageOffset := 0
		total := 1

		for total > pageOffset {
			requestParams.Set("offset", strconv.Itoa(pageOffset))

			err, count := aac.retryRequest(ctx, requestParams, output, reportChan)
			if err != nil {
				return err
			}
			total = count
			pageOffset = pageOffset + pageSize
		}
	}
	return nil
}

func (aac *AtokaApiClient) retryRequest(ctx context.Context, params url.Values, output chan<- AAItem, reportChan chan<- ReportEntry) (error, int) {
	for i := 0; i < aac.conf.MaxRetries; i++ {
		response, err := aac.performRequest(aac.downloadEndpoint, params)
		if err == nil {
			for _, item := range response.Items {
				select {
				case output <- item:
					key := aac.extractKey(item)
					reportChan <- ReportEntry{key, ReportFound}
				case <-ctx.Done():
					return ctx.Err(), 0
				}
			}
			return nil, response.Meta.Count
		} else {
			if apiErr, ok := err.(*APIError); ok {
				if apiErr.Fatal {
					return apiErr, 0
				}
			}
		}

		time.Sleep(aac.conf.WaitBeforeRetry)
	}
	return nil, 0
}

func (aac *AtokaApiClient) RequestMaster(ctx context.Context, g *errgroup.Group, args DownloadArgs, chunkedTaxIds <-chan string, reportChan chan ReportEntry) <-chan AAItem {
	var (
		wg     sync.WaitGroup
		params = url.Values{}
		items  = make(chan AAItem, 100)
	)

	params.Set("token", *args.GlobalArgs.Token)
	params.Set("packages", strings.Join(*args.Packages, ","))
	params.Set("limit", strconv.Itoa(aac.conf.MaxLimit))

	for i := 0; i < aac.conf.NumWorkers; i++ {
		wg.Add(1)
		g.Go(func() error {
			return aac.requestWorker(ctx, &wg, chunkedTaxIds, params, items, reportChan)
		})
	}

	g.Go(func() error {
		wg.Wait()
		close(items)
		close(reportChan)
		return nil
	})

	return items
}

func (aac *AtokaApiClient) UploadRequest(ctx context.Context, g *errgroup.Group, args UploadArgs, path string) error {
	var (
		body   bytes.Buffer
		writer = multipart.NewWriter(&body)
		params = url.Values{}
	)
	writer.SetBoundary("boundary")
	writeFileParam(writer, "file", path)
	writer.Close()

	params.Set("token", *args.GlobalArgs.Token)
	params.Set("type", aac.docType)

	_, err := aac.performUploadRequest(body, *args.Portfolio, params, writer.FormDataContentType())

	return err
}

func writeFileParam(writer *multipart.Writer, key string, value string) (err error) {
	var (
		fileContent *os.File
		fileInfo    os.FileInfo
		formWriter  io.Writer
	)

	fileContent, err = os.Open(value)
	if err != nil {
		return
	}
	defer fileContent.Close()

	fileInfo, err = fileContent.Stat()
	if err != nil {
		return
	}

	formWriter, err = writer.CreateFormFile(key, fileInfo.Name())
	if err != nil {
		return
	}

	if _, err = io.Copy(formWriter, fileContent); err != nil {
		return
	}
	return
}

func (aac *AtokaApiClient) CheckCustomDataExists(args *GlobalArgs, customData, collectionType string) error {

	params := url.Values{}

	params.Set("token", *args.Token)
	params.Set("type", collectionType)

	res, err := aac.Client.Get(aac.conf.BaseUrl + "/v2/customdata/" + customData + "?" + params.Encode())
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusOK {
		return NewCustomDataError(res.StatusCode)
	}

	return nil
}

func (aac *AtokaApiClient) IncreaseCustomDataVersion(args *GlobalArgs, customData, collectionType string) (aaitem AACustomDataResponse, err error) {

	params := url.Values{}
	params.Set("token", *args.Token)

	data := url.Values{}
	data.Set("type", collectionType)

	res, err := aac.Client.PostForm(aac.conf.BaseUrl+"/v2/customdata/"+customData+"/versions/increase?"+params.Encode(), data)
	if err != nil {
		return
	}

	if res.StatusCode != http.StatusOK {
		err = NewCustomDataError(res.StatusCode)
		return
	}

	bodyData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	err = json.Unmarshal(bodyData, &aaitem)

	return
}

func (aac *AtokaApiClient) GetCustomDataVersion(args *GlobalArgs, customData, collectionType string) (aaitem AACustomDataResponse, err error) {
	params := url.Values{}
	params.Set("token", *args.Token)
	params.Set("type", collectionType)

	res, err := aac.Client.Get(aac.conf.BaseUrl + "/v2/customdata/" + customData + "/versions?" + params.Encode())
	if err != nil {
		return
	}

	if res.StatusCode != http.StatusOK {
		err = NewCustomDataError(res.StatusCode)
		return
	}

	bodyData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	err = json.Unmarshal(bodyData, &aaitem)

	return aaitem, err
}

func (aac *AtokaApiClient) GetStatusForAll(args *GlobalArgs, collectionType string) (response ProgressResponse, err error) {
	var (
		requestUrl     = fmt.Sprintf("%s%s/bulk/progress?", aac.conf.BaseUrl, CustomDataEndpoint)
	)
	params := url.Values{}
	params.Set("token", *args.Token)
	params.Set("type", collectionType)

	res, err := aac.Client.Get(requestUrl + params.Encode())
	if err != nil {
		return
	}

	if res.StatusCode != http.StatusOK {
		err = NewCustomDataError(res.StatusCode)
		return
	}

	bodyData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	err = json.Unmarshal(bodyData, &response)

	return response, err
}

func (aac *AtokaApiClient) GetStatusForCollection(args *GlobalArgs, collection, collectionType string) (response ProgressResponse, err error) {
	var (
		schemaResponse ProgressSchemaResponse
		requestUrl     = fmt.Sprintf("%s%s/bulk/progress/%s?", aac.conf.BaseUrl, CustomDataEndpoint, collection)
	)

	params := url.Values{}
	params.Set("token", *args.Token)
	params.Set("type", collectionType)

	res, err := aac.Client.Get(requestUrl + params.Encode())
	if err != nil {
		return
	}

	if res.StatusCode != http.StatusOK {
		err = NewCustomDataError(res.StatusCode)
		return
	}

	bodyData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(bodyData, &schemaResponse)

	response.Items = make(map[string][]ProgressResponseItem)

	response.Items[collection] = schemaResponse.Items

	return response, err
}
