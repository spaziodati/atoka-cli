// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"golang.org/x/sync/errgroup"
)

var _ = Describe("upload file format", func() {
	var (
		ctx   context.Context
		g     *errgroup.Group
		items []*CustomDataInputItem

		fileContent []string
		err         error
	)
	BeforeEach(func() {
		ctx = context.Background()
		g, ctx = errgroup.WithContext(ctx)
		fileContent = nil
		err = nil
	})

	JustBeforeEach(func() {
		var outPath string

		outPath, err = PrepareFile(ctx, g, items, true)
		if err == nil {
			fileContent = make([]string, 0)

			reader, _ := os.Open(outPath)
			defer reader.Close()

			scanner := bufio.NewScanner(reader)
			for scanner.Scan() {
				row := scanner.Text()
				fileContent = append(fileContent, row)
			}
		}
	})

	Context("input only perimetro", func() {

		BeforeEach(func() {
			items = []*CustomDataInputItem{
				{
					"MYID1",
					map[string]string{
						"taxId": "myTaxId",
					},
				},
				{
					"MYID2",
					map[string]string{
						"taxId": "myTaxId2",
					},
				},
			}
		})

		It("should write header", func() {
			Expect(fileContent).To(HaveLen(len(items) + 1))
			Expect(fileContent[0]).To(Equal("key,taxId"))
		})

		It("should write 2 cols", func() {
			Expect(fileContent).To(Not(BeNil()))
			for _, row := range fileContent {
				Expect(strings.Split(row, ",")).To(HaveLen(2))
			}
		})

	})

	Context("input with custom data", func() {

		BeforeEach(func() {
			items = []*CustomDataInputItem{
				{
					"MYID1",
					map[string]string{
						"taxId": "myTaxId1",
						"key1":  "value1",
					},
				},
				{
					"MYID2",
					map[string]string{
						"taxId": "myTaxId2",
						"key1":  "value2",
					},
				},
			}
		})

		It("should write header", func() {
			Expect(fileContent).To(HaveLen(len(items) + 1))
			Expect(fileContent[0]).To(Equal("key,key1,taxId"))
		})

		It("should write 3 cols", func() {
			Expect(fileContent).To(Not(BeNil()))
			for _, row := range fileContent {
				Expect(strings.Split(row, ",")).To(HaveLen(3))
			}
		})
		It("should have ordered columns", func() {
			for i, row := range fileContent {
				if i > 0 {
					Expect(row).To(ContainSubstring(fmt.Sprintf("value%d,myTaxId%d", i, i)))
				}
			}
		})

	})
})
