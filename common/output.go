// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"golang.org/x/sync/errgroup"
)

// This function writes the output, returning the number of bytes written and (possibly) an error
func flushOutput(outputPath string, items []AAItem, limit, offset int) (int64, error) {
	outputMap := map[string]interface{}{
		"items": items,
	}
	meta := map[string]int{
		"count": len(items),
	}

	if limit > 0 {
		meta["limit"] = limit
		meta["offset"] = offset
	}

	outputMap["meta"] = meta

	outputBytes, err := json.Marshal(outputMap)
	if err != nil {
		return 0, err
	}

	f, err := os.Create(outputPath)
	if err != nil {
		return 0, err
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	totalBytesWritten, err := w.Write(outputBytes)
	if err != nil {
		return 0, err
	}
	w.Flush()
	return int64(totalBytesWritten), nil
}

func serializeMapToJson(outputMap interface{}, path string) (int, error) {
	outputBytes, err := json.MarshalIndent(outputMap, "", "    ")
	if err != nil {
		return 0, err
	}

	f, err := os.Create(path)
	if err != nil {
		return 0, err
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	bytesWritten, err := w.Write(outputBytes)

	w.Flush()
	return bytesWritten, err
}

func WriteOutput(ctx context.Context, g *errgroup.Group, itemsChan <-chan AAItem, outputPath string, pagesize int, silent bool) {
	g.Go(func() error {
		var (
			outputFileCount         = 0
			totalBytesWritten int64 = 0
			ext                     = ""
			items                   = make([]AAItem, 0, pagesize)
		)
		if strings.HasSuffix(outputPath, ".json") {
			outputPath = strings.TrimSuffix(outputPath, ".json")
			ext = ".json"
		}

		for item := range itemsChan {
			items = append(items, item)
			if pagesize > 0 && len(items) >= pagesize {
				bytesWritten, err := flushOutput(fmt.Sprintf("%s.%d%s", outputPath, outputFileCount, ext), items, pagesize, pagesize*outputFileCount)
				if err != nil {
					return err
				}
				totalBytesWritten += bytesWritten
				outputFileCount++
				items = nil
			}
		}
		// Continue only if we have something to output
		if len(items) == 0 {
			println("terminating")
			return ctx.Err()
		}

		if outputFileCount == 0 {
			outputPath = fmt.Sprintf("%s%s", outputPath, ext)
		} else {
			outputPath = fmt.Sprintf("%s.%d%s", outputPath, outputFileCount, ext)
		}
		bytesWritten, err := flushOutput(outputPath, items, pagesize, pagesize*outputFileCount)
		if err != nil {
			return err
		}
		totalBytesWritten += bytesWritten

		if !silent {
			if outputFileCount > 0 {
				fmt.Printf("wrote %d outputs with total size of %s\n", outputFileCount+1, bytesToHuman(totalBytesWritten))
			} else {
				fmt.Printf("wrote output of %s to %s\n", bytesToHuman(int64(totalBytesWritten)), outputPath)
			}
		}

		return nil
	})
}

/// This function relies on the assumption that all valid cfs are first passed as
/// reportNotFound and then possibly passed again as reportFound if the request worker
/// has received a match from the API
func WriteDownloadReport(ctx context.Context, g *errgroup.Group, reportChan <-chan ReportEntry, reportPath string, silent bool) {
	g.Go(func() error {

		var (
			reportItems                                                                   = map[string]string{}
			input_size, invalid, duplicate, not_found, found, found_multiple, output_size uint
		)

		for item := range reportChan {
			input_size++

			switch item.Status {
			case ReportFound:
				input_size-- // it was already counted as a reportNotFound
				output_size++

				current_status, ok := reportItems[item.Key]

				if ok {
					switch current_status {
					case ReportNotFound:
						reportItems[item.Key] = item.Status
						found++
						not_found--

					case ReportFound:
						found--
						found_multiple++
						reportItems[item.Key] = ReportFoundMultiple
					}
				}

			case ReportNotFound:
				not_found++
				reportItems[item.Key] = item.Status

			case ReportInvalid:
				invalid++
				reportItems[item.Key] = item.Status

			case ReportDuplicate:
				duplicate++
			}
		}

		// Continue only if there is something to write in the report
		if output_size == 0 && ctx.Err() != nil {
			return nil
		}

		metaMap := map[string]uint{
			"input_size":     input_size,     // total taxId in input
			"invalid":        invalid,        // # invalid taxId
			"duplicates":     duplicate,      // # tax ids duplicated in input
			"not_found":      not_found,      // # tax ids not found in atoka
			"found":          found,          // # tax ids that match exactly 1 person in atoka
			"found_multiple": found_multiple, // # tax ids that matched more than 1 person in atoka
			"output_size":    output_size,    // total test_data found
		}

		outputMap := map[string]interface{}{
			"meta":  metaMap,
			"items": reportItems,
		}
		bytesWritten, err := serializeMapToJson(outputMap, reportPath)
		if err != nil {
			return err
		}

		if !silent {
			fmt.Println("Report summary:")
			fmt.Printf("%18s: %12d\n", "input_size", input_size)
			fmt.Printf("%18s: %12d\n", "invalid", invalid)
			fmt.Printf("%18s: %12d\n", "duplicate", duplicate)
			fmt.Printf("%18s: %12d\n", "not_found", not_found)
			fmt.Printf("%18s: %12d\n", "found", found)
			fmt.Printf("%18s: %12d\n", "found_multiple", found_multiple)
			fmt.Printf("%18s: %12d\n\n", "output_size", output_size)

			fmt.Printf("wrote report of %s to %s\n", bytesToHuman(int64(bytesWritten)), reportPath)
		}
		return nil
	})
}

func WriteCustomDataReport(ctx context.Context, g *errgroup.Group, reportChan <-chan ReportEntry, reportPath string, silent bool) {
	g.Go(func() error {

		var (
			reportItems = map[string]string{}
			inputSize   uint
			metaMap     = map[string]uint{}
		)

		for item := range reportChan {
			inputSize++

			metaMap[item.Status] += 1
			reportItems[item.Key] = item.Status
		}

		// Continue only if there is something to write in the report
		if len(metaMap) == 0 && ctx.Err() != nil {
			return nil
		}

		metaMap["input_size"] = inputSize

		outputMap := map[string]interface{}{
			"meta":  metaMap,
			"items": reportItems,
		}
		bytesWritten, err := serializeMapToJson(outputMap, reportPath)
		if err != nil {
			return err
		}

		if !silent {
			fmt.Println("Input processed:")
			fmt.Printf("%14s: %12d\n", "input_size", inputSize)
			fmt.Printf("%14s: %12d\n", "invalid", metaMap[ReportInvalid])
			fmt.Printf("%14s: %12d\n", "duplicate", metaMap[ReportDuplicate])
			fmt.Printf("%14s: %12d\n", "valid", metaMap[ReportValid])
			fmt.Printf("wrote report of %s to %s\n", bytesToHuman(int64(bytesWritten)), reportPath)
		}
		return nil
	})
}

func bytesToHuman(nBytes int64) string {
	if nBytes > 100000000 {
		return fmt.Sprintf("%.2f gb", float64(nBytes)/1000000000)
	}
	if nBytes > 100000 {
		return fmt.Sprintf("%.2f mb", float32(nBytes)/1000000.0)
	}
	if nBytes >= 1000 {
		return fmt.Sprintf("%.2f kb", float32(nBytes)/1000.0)
	} else {
		return fmt.Sprintf("%d bytes", nBytes)
	}
}
