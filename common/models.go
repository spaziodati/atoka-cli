// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import "time"

const (
	baseUrl                  = "https://api.atoka.io"
	CustomDataEndpoint       = "/v2/customdata"
	CustomDataUploadEndpoint = CustomDataEndpoint + "/bulk"

	maxRate  = 10 // requests per second
	maxLimit = 50

	ReportFound         = "found"
	ReportFoundMultiple = "found_multiple"
	ReportNotFound      = "not_found"
	ReportInvalid       = "invalid"
	ReportValid         = "valid"
	ReportDuplicate     = "duplicate"
)

type ReportEntry struct {
	Key    string
	Status string
}

type AAResponse struct {
	Error   bool
	Message string
	Code    string
	Items   []AAItem
	Meta    AAMeta
}

type AAMeta struct {
	Count    int
	Limit    int
	Offset   int
	Ordering string
}

type AAItem map[string]interface{}

type AAConf struct {
	BaseUrl            string
	CustomDataEndpoint string
	HttpDefaultTimeout time.Duration
	MaxRetries         int
	WaitBeforeRetry    time.Duration
	NumWorkers         int
	MaxLimit           int
}

type AACustomDataResponse struct {
	Item AACustomDataInfo
}

type AACustomDataInfo struct {
	Id       string
	Label    string
	Type     string
	Versions []AACustomDataVersion
}

type AACustomDataVersion struct {
	Active  bool
	Version string
}

func DefaultConfig(args GlobalArgs) AAConf {
	apiUrl := baseUrl
	if args.BaseUrl != nil && *args.BaseUrl != "" {
		apiUrl = *args.BaseUrl
	}
	return AAConf{
		apiUrl,
		CustomDataUploadEndpoint,
		time.Second * 60,
		3,
		1 * time.Second,
		4,
		maxLimit,
	}
}

type CustomDataInputItem struct {
	Key  string
	Data map[string]string
}

// Computes the size in bytes that this CustomDataInputItem
// takes in the csv that is uploaded to the api
func (item *CustomDataInputItem) sizeInBytes() int {
	var size = 0

	size += len(item.Key)
	for _, data := range item.Data {
		size += 1 // count a comma
		size += len(data)
	}
	size += 1 // count the end of the line

	return size
}

func (item *CustomDataInputItem) headerSize() int {
	var size = 0

	size += len("key")
	for key := range item.Data {
		size += 1 // count a comma
		size += len(key)
	}
	size += 1 // count the end of the line

	return size
}
