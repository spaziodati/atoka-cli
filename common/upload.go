// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package common

import (
	"context"
	"encoding/csv"
	"io/ioutil"

	"sort"

	"fmt"
	"os"

	"golang.org/x/sync/errgroup"
)

func PrepareFile(ctx context.Context, g *errgroup.Group, taxIds []*CustomDataInputItem, silent bool) (filename string, err error) {
	writeHeader := true
	tmpfile, _ := ioutil.TempFile("", "file_upload")
	filename = tmpfile.Name()

	writer := csv.NewWriter(tmpfile)

	var sortedKeys []string
	// write transformed tax ids to file
	for _, item := range taxIds {
		// write header for the first row
		if writeHeader {
			header := []string{"key"}
			sortedKeys = make([]string, 0, len(item.Data))

			for key := range item.Data {
				sortedKeys = append(sortedKeys, key)
			}

			sort.Strings(sortedKeys)
			header = append(header, sortedKeys...)
			if err = writer.Write(header); err != nil {
				return
			}

			writeHeader = false
		}

		values := []string{item.Key}
		for _, key := range sortedKeys {
			values = append(values, item.Data[key])
		}

		if err = writer.Write(values); err != nil {
			return
		}
	}
	writer.Flush()

	if err = tmpfile.Close(); err != nil {
		return
	}
	return
}

func ExecuteGenericUpload(args UploadArgs, apiClient AtokaApiClient, inputProcessor InputProcessor, collectionType string) {
	if !*args.GlobalArgs.Silent {
		println("Client version:", *args.GlobalArgs.Version)
		println("Using API token:", *args.GlobalArgs.Token)
		println("Report file will be saved at", *args.Report)
		println()
	}

	ctx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()

	g, ctx := errgroup.WithContext(ctx)


	var (
		apiErr error
		customDataInfo AACustomDataResponse
	)

	if *args.AppendMode {
		customDataInfo, apiErr = apiClient.GetCustomDataVersion(args.GlobalArgs, *args.Portfolio, collectionType)
	} else {
		customDataInfo, apiErr = apiClient.IncreaseCustomDataVersion(args.GlobalArgs, *args.Portfolio, collectionType)
	}
	
	if apiErr != nil {
		fmt.Println("ERROR", apiErr.Error())
		return
	}

	if len(customDataInfo.Item.Versions) > 0 {
		println("Using custom data version: ", customDataInfo.Item.Versions[0].Version)
	}

	apiErr = apiClient.CheckCustomDataExists(args.GlobalArgs, *args.Portfolio, collectionType)

	if apiErr != nil {
		println("Error:", apiErr.Error())
		return
	}

	reportChan := make(chan ReportEntry, 100)

	// read input
	taxIds := inputProcessor.ProcessInput(ctx, g, reportChan)

	// Start the goroutine for write report, so that it consumes entries from reportChan
	WriteCustomDataReport(ctx, g, reportChan, *args.Report, *args.GlobalArgs.Silent)

	// prepare file to send to the api
	chunkedTaxIds := ChunkOfMaxBytes(ctx, g, taxIds, 900000) // auth proxy accepts max 1mb, leaving 100kb for the headers, etc.

	for chunk := range chunkedTaxIds {
		apiErr = ProcessChunk(ctx, g, chunk, &apiClient, args)
		if apiErr != nil {
			fmt.Println("ERROR", apiErr.Error())
			return
		}
	}

	close(reportChan)

	err := g.Wait()
	if err != nil {
		fmt.Println(err.Error())
	}
}

func ProcessChunk(ctx context.Context, g *errgroup.Group, taxIds []*CustomDataInputItem, apiClient *AtokaApiClient, args UploadArgs) error {
	// prepare file to send to the api
	tmpFile, fileErr := PrepareFile(ctx, g, taxIds, *args.GlobalArgs.Silent)

	defer os.Remove(tmpFile)

	if fileErr != nil {
		return fileErr
	}

	// send request
	apiErr := apiClient.UploadRequest(ctx, g, args, tmpFile)

	return apiErr
}
