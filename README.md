# Atoka - The Command Line Interface

`atoka` is a command line tool that allows you to perform some simple actions against the
[Atoka API](https://atoka.io/it/atoka-api/). No coding, no headache 😎


## Getting started

Getting started with `atoka` is easy, but it requires a few steps.


### Install

First of all, you have to download the right `atoka` client for you OS from this list:

- [Linux 64bit](https://public-bin.spaziodati.eu/atoka-bulk-client/latest/linux64.zip)
- [Linux 32bit](https://public-bin.spaziodati.eu/atoka-bulk-client/latest/linux32.zip)
- [macOS 64bit](https://public-bin.spaziodati.eu/atoka-bulk-client/latest/mac64.zip)
- [Windows 64bit](https://public-bin.spaziodati.eu/atoka-bulk-client/latest/windows64.zip)
- [Windows 32bit](https://public-bin.spaziodati.eu/atoka-bulk-client/latest/windows32.zip)

and put it in your favourite folder!


### How to get a secret token

In order to use the client you have to provide a valid [Atoka API](https://atoka.io/it/atoka-api/) token, which usually looks like this `d05dbda5d4ea49edb35e6130506ac815`. To get one write to [sales@atoka.io](mailto:sales@atoka.io)

You can either pass it using the `-t --token` flag,

**windows**
```commandline
atoka.exe --token <your-token> ping
```

**linux & mac os**
```bash
./atoka --token <your-token> ping
```
or to avoid passing the flag every time you can set the `ATOKEN` environmental variable to pass it automatically, like this

**window**
```commandline
SET ATOKEN=<your-token>
```

**linux & mac os**
```bash
export ATOKEN=<your-token>
./atoka ping
```
or this (only linux & mac os)
```bash
ATOKEN=<your-token> ./atoka ping
```
Every other example in this readme will use the flag `--token` and `d05dbda5d4ea49edb35e6130506ac815` as an example token.

### Did I do everything right?

We hate to waste your time so, before going crazy, let's check that everything is up and running.
Open the prompt and run these commands

**windows (cmd)**
```commandline
C:\Users\... > cd <atoka-folder>
C:\Users\..\<atoka-folder> > atoka ping --token d05dbda5d4ea49edb35e6130506ac815
Successfully pinged Atoka servers.
Everything is fine.
```
**linux & mac os**
```sh
$ cd <atoka-folder>
$ ./atoka ping --token d05dbda5d4ea49edb35e6130506ac815
Successfully pinged Atoka servers.
Everything is fine.
```

Congrats! 👍  Easy, right?

You're now up and running with `atoka`. There are a variety of things you can do to start using the tool. Check out our use cases!


## Use Case: From Tax code to Data

Do you have a tax code and are you wondering who's this person?

**windows(cmd)**

Save your tax code in a file called *input.txt* in the same folder of *atoka.exe*.
```commandline
C:\Users\...\ > cd <atoka-folder>
C:\Users\...\<atoka-folder> > atoka --token d05dbda5d4ea49edb35e6130506ac815 people download input.txt
```

**linux & mac os**
```sh
$ cd <atoka-folder>
$ echo "<TAX_CODE>" > input.txt
$ ./atoka --token d05dbda5d4ea49edb35e6130506ac815 people download input.txt
```

**Result**

a new file named `output_<NOW>.json` appears in the same folder, check it out: this is just a sample of the content

```json
{
  "items": [
    {
      "country": "it",
      "id": "pABC1234dsad1jkfhdak",
      "name": "Mario Rossi",
      "base": {
         "data": "here"
      }
    }
  ],
  "meta": {
    "count": 1,
    "offset": 0,
    "limit": 1000
  }
}
```

Of course, this is the simplest example ever. More tax codes can be provided by adding them to 'input.txt' one for each line:

```sh
tax code 1
tax code 2
tax code 3
...
```

An MS Excel file format (`.xlsx`) works as well: insert the tax codes in the first column; no header required.


### Advanced Usage

```sh
atoka people download INPUT --token TOKEN [--packages PACKAGES] [--output OUTPUT] [--report REPORT] [--silent]
```

| Param | Description | Default |
| --- |---| ---|
| `INPUT` | path to the input file containing people identifiers (e.g. tax codes)  |  |
| `--token TOKEN, -t TOKEN` | [see "How to get a secret token"](#how-to-get-a-secret-token) |  |
| `--packages PACKAGES`, `-p PACKAGES` | package to retrieve, can be specified multiple times, [read more](https://developers.atoka.io) | `*` |
| `--output FILE`, `-o FILE` | path to the results file | `./output_<NOW>.json` |
| `--report REPORT`, `-r REPORT` | path to the report file | `./report_<NOW>.json` |
| `--pagesize PAGESIZE`, `-P PAGESIZE` | paginate output in files of PAGESIZE elements. Setting it to 0 disables pagination | 100000 |
| `--silent`, `-s` | don't print on the terminal | |
| `--help`, `-h` | display the help message and exit | |
| `--version` | display version and exit | |


### Examples

Do you want to **find duplicated customers** in your user base?

**input.txt**
```sh
VALIDTAXCODE001
INVALIDTAXCODE1
VALIDTAXCODE002
VALIDTAXCODE001
NOTFOUNDTAXCODE
```

**windows(cmd)**
```commandline
C:\Users..\<atoka-folder> > atoka.exe people download input.txt --token d05dbda5d4ea49edb35e6130506ac815 --report the-report.json --silent
```

**linux & mac os**
```sh
$ ./atoka people download input.txt --token d05dbda5d4ea49edb35e6130506ac815 --report the-report.json --silent
```

**Result: report.json**
```json
{
    "items": {
        "VALIDTAXCODE001": "found",
        "INVALIDTAXCODE1": "invalid",
        "VALIDTAXCODE002": "found",
        "VALIDTAXCODE001": "duplicate",
        "NOTFOUNDTAXCODE": "not_found",
    },
    "meta": {
        "input_size": 5,
        "invalid": 1,
        "duplicates": 1,
        "not_found": 1,
        "found": 2,
        "found_multiple": 0,
        "output_size": 2
    }
}
```

Let's try another one: **export full name and birth date** of your customers (you will need to have [jq](https://stedolan.github.io/jq/) installed)

**windows(cmd)**
```commandline
C:\Users\...\<atoka-folder> > atoka.exe people download input.txt --token d05dbda5d4ea49edb35e6130506ac815 --packages base --output out.json
C:\Users\...\<atoka-folder> > jq.exe ".items[].base | .taxId + \"\t\" + .givenName + \" \" + .familyName + \"\t\" + .birthDate" out.json
```

**linux & mac os**
```sh
$ ./atoka people download input.txt --token d05dbda5d4ea49edb35e6130506ac815 --packages base --output out.json
$ cat out.json | jq -r '.items[].base | .taxId + "\t" + .givenName + " " + .familyName + "\t" + .birthDate'
```

**Result output**
```
VALIDTAXCODE001   name1 surname1  1900-01-01
VALIDTAXCODE002   name2 surname2  1900-01-01
...
```

## DIY

You can find the source code for Atoka CLI at https://gitlab.com/spaziodati/atoka-cli

### [Windows] Create a script file for launching the atoka command

In the same directory of `atoka.exe` create a file `atoka.bat` and open it with a text editor (not Microsoft Word, notepad is fine) and fill it with the following:
```commandline
SET ATOKEN=<your-token>

atoka.exe people download <input-file>

set /p temp="Hit enter to continue"
```
Make sure to write your [API token](#how-to-get-a-secret-token) in place of `<your-token>` and your choosen input file name in place of `<input-file>` (for example `input.txt`).
You can add any other [options](#advanced-usage) you want to the `atoka.exe` command like for example `--packages` or `--page-size`.

The last line is just needed to keep the window open once it finishes.

Now you just need to place the input file in the same folder and double click on `atoka.bat` to launch `atoka` and create the report and output jsons.




### Installing dependencies
Make sure to have a working installation of [Go](https://golang.org/doc/install) and
[Dep](https://github.com/golang/dep#setup) and that the code is in the right folder in your
[GOPATH](https://github.com/golang/go/wiki/SettingGOPATH) `$GOPATH/src/spaziodati.eu/atoka`, then just run
`dep ensure` from that folder.

```sh
$ export GOPATH=/src/spaziodati.eu/atoka
$ dep ensure
```

### Building binaries

In the [repo](https://gitlab.com/spaziodati/atoka-cli) there is a makefile with target for:
* **all** build all binaries
* **windows** binary for windows 64 bit
* **mac** binary for mac 64 bit
* **linux** binary for linux 64 bit
* **windows32** binary for windows 32 bit
* **linux32** binary for linux 32 bit

```sh
$ make all
```
