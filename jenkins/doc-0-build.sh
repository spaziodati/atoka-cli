#!/bin/bash

set -eux

rm -rf doc
mkdir doc
pushd doc
ln ../README.md .

dockerfile=Dockerfile-doc.tmp

cat > ${dockerfile} <<EOF
FROM python:3.6.1-slim

RUN pip install -q grip

WORKDIR /workspace
CMD ["grip", "README.md", "--export", "README.html"]
EOF

DOC_IMAGE=${COMPOSE_PROJECT_NAME}_doc

docker build -f ${dockerfile} -t $DOC_IMAGE .
docker run --rm --mount type=bind,src=$(pwd),dst=/workspace $DOC_IMAGE

rm -rf "${dockerfile}"
popd
