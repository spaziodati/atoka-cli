#!/usr/bin/env bash

set -eux

S3_BUCKET_URL="s3://public-bin.spaziodati.eu/atoka-bulk-client"
for tag in "${TAG_NOW}" "latest"; do
   aws s3 sync distrib "${S3_BUCKET_URL}/${tag}"
done
