#!/usr/bin/env bash

set -eux

S3_BUCKETS="s3://developers.atoka.io s3://developers-u.atoka.io"
S3_PATH="atoka-cli/index.html"
for bucket in ${S3_BUCKETS}; do
  aws s3 cp doc/README.html "${bucket}/${S3_PATH}"
done
