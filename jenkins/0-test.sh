#!/usr/bin/env bash

set -eux

export COMPOSE_FILE="docker-compose-jenkins.yml"

docker-compose rm -f
docker-compose build
docker-compose up --no-color --timeout 1 --no-build -d
docker-compose run go docker/test.sh
docker-compose run go docker/build.sh
docker-compose stop

