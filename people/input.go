// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package people

import (
	"context"
	"regexp"
	"strings"

	"github.com/stellamargonar/gocodicefiscale"
	"golang.org/x/sync/errgroup"
	"spaziodati.eu/atoka-cli/common"
)

const (
	defaultColId = "taxId"
	sep          = ","
)

func chunk(ctx context.Context, g *errgroup.Group, input <-chan *common.CustomDataInputItem, size int) <-chan string {
	output := make(chan string)

	g.Go(func() error {
		defer close(output)

		if size <= 0 {
			panic("Chunk size must be greater than 0")
		}

		accum := make([]string, 0, size)

		for item := range input {
			taxId := item.Key
			accum = append(accum, taxId)

			if len(accum) == size {
				select {
				case output <- strings.Join(accum, sep):
				case <-ctx.Done():
					return ctx.Err()
				}
				accum = accum[:0]
			}
		}
		if len(accum) > 0 {
			select {
			case output <- strings.Join(accum, sep):
			case <-ctx.Done():
				return ctx.Err()

			}
		}

		return nil
	})

	return output
}

func NewPersonInputProcessor(inputFile string, defaultReportStatus string, silent bool) *common.InputProcessor {
	return &common.InputProcessor{
		MatchFieldName:      defaultColId,
		ValidateId:          gocodicefiscale.ValidCF,
		MatchDataId:         validateTaxId,
		TransformId:         transformTaxId,
		InputFile:           inputFile,
		Silent:              silent,
		DefaultReportStatus: defaultReportStatus,
	}
}

func transformTaxId(taxId string) string {
	return strings.TrimSpace(strings.ToUpper(taxId))
}

func validateTaxId(taxId string) bool {
	match, _ := regexp.MatchString("([A-Z]|[0-9]){16}", taxId)
	return match
}
