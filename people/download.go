// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package people

import (
	"context"
	"fmt"
	"strings"

	"golang.org/x/sync/errgroup"
	"spaziodati.eu/atoka-cli/common"
)

func ExecuteDownload(conf AAConf, args common.DownloadArgs) {
	if !*args.GlobalArgs.Silent {
		println("Client version:", *args.GlobalArgs.Version)
		println("Using API token:", *args.GlobalArgs.Token)
		println("Fetching packages:", strings.Join(*args.Packages, ","))
		println("Report file will be saved at", *args.Report)

		if *args.PageSize > 0 {
			ext := ""
			if strings.HasSuffix(*args.Output, ".json") {
				ext = ".json"
			}

			fmt.Printf("Output files of size %d elements will be saved at %s.[1..]%s\n", *args.PageSize, strings.TrimSuffix(*args.Output, ".json"), ext)
		} else {
			println("Output file will be saved at", *args.Output)
		}
		println()
	}

	ctx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()

	g, ctx := errgroup.WithContext(ctx)

	reportChan := make(chan common.ReportEntry, 100)

	inputProcessor := NewPersonInputProcessor(*args.Input, common.ReportNotFound, *args.GlobalArgs.Silent)

	taxIds := inputProcessor.ProcessInput(ctx, g, reportChan)

	chunkedTaxIds := chunk(ctx, g, taxIds, conf.MaxLimit)

	peopleApiClient := NewPeopleApiClient(conf)
	aaitems := peopleApiClient.RequestMaster(ctx, g, args, chunkedTaxIds, reportChan)

	common.WriteOutput(ctx, g, aaitems, *args.Output, *args.PageSize, *args.GlobalArgs.Silent)
	common.WriteDownloadReport(ctx, g, reportChan, *args.Report, *args.GlobalArgs.Silent)

	err := g.Wait()

	if err != nil {
		fmt.Println(err.Error())
	}
}
