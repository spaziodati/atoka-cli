// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package people

import (
	"context"
	"io/ioutil"
	"net/http"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/ghttp"
	"golang.org/x/sync/errgroup"
	"spaziodati.eu/atoka-cli/common"

)

var _ = Describe("people upload", func() {
	var (
		ctx         context.Context
		g           *errgroup.Group
		server      *ghttp.Server
		conf        AAConf
		args        common.UploadArgs
		apiPageSize string
		apiClient   *common.AtokaApiClient
		filePath    string
		err         error
		token       = "test-token"
		portfolio   = "eni"
	)

	BeforeEach(func() {
		ctx = context.Background()
		g, ctx = errgroup.WithContext(ctx)
		server = ghttp.NewServer()

		apiPageSize = "50"
		filePath = ""

		conf = AAConf{
			AAConf: common.AAConf{
				BaseUrl:            server.URL(),
				CustomDataEndpoint: common.CustomDataUploadEndpoint,
				HttpDefaultTimeout: time.Second * 60,
				MaxRetries:         3,
				WaitBeforeRetry:    500 * time.Millisecond,
				NumWorkers:         4,
				MaxLimit:           50,
			},
			peopleEndpoint: peopleEndpoint,
		}
		apiClient = NewPeopleApiClient(conf)
	})

	JustBeforeEach(func() {
		report, tmpErr := ioutil.TempFile("", "test_people_upload")
		if tmpErr != nil {
			panic(tmpErr)
		}
		reportName := report.Name()
		silent := true
		input := GetTestFilePath(filePath)
		args = common.UploadArgs{
			CommonArgs: common.CommonArgs{
				GlobalArgs: &common.GlobalArgs{Token: &token, Silent: &silent},
				Input:      &input,
				Output:     nil,
				Report:     &reportName,
			},
			Portfolio: &portfolio,
		}

		err = apiClient.UploadRequest(ctx, g, args, GetTestFilePath(filePath))
	})

	Context("token authorized", func() {
		BeforeEach(func() {
			bodyData, err := ioutil.ReadFile(GetTestFilePath("valid_upload_multipart.txt"))
			if err != nil {
				panic(err)
			}

			server.AppendHandlers(
				ghttp.CombineHandlers(
					ghttp.VerifyRequest("POST", "/v2/customdata/bulk/eni"),
					ghttp.VerifyFormKV("token", token),
					ghttp.VerifyFormKV("type", "person"),
					ghttp.VerifyHeaderKV("Content-Type", "multipart/form-data; boundary=boundary"),
					ghttp.VerifyBody(bodyData),
					ghttp.RespondWith(200, "{}"),
				))

			filePath = "valid_upload_tmp.txt"
		})

		It("should upload data to the api", func() {
			Expect(err).To(BeNil())
		})

	})

	Context("token not authorized", func() {
		BeforeEach(func() {
			filePath = "valid_upload_tmp.txt"

			bodyData, err := ioutil.ReadFile(GetTestFilePath("valid_upload_multipart.txt"))
			if err != nil {
				panic(err)
			}

			server.AppendHandlers(
				ghttp.CombineHandlers(
					ghttp.VerifyRequest("POST", "/v2/customdata/bulk/eni"),
					ghttp.VerifyFormKV("token", token),
					ghttp.VerifyFormKV("type", "person"),
					ghttp.VerifyHeaderKV("Content-Type", "multipart/form-data; boundary=boundary"),
					ghttp.VerifyBody(bodyData),
					ghttp.RespondWith(http.StatusUnauthorized, "{}"),
				))
		})

		It("should return an error if the token is not authorized", func() {
			Expect(err).To(Not(BeNil()))
			Expect(err.Error()).Should(Equal("The token you're using is not authorized to use this command"))
		})

		It("should write not authorized in the report", func() {

		})
	})
})
