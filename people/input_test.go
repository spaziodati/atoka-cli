// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package people

import (
	"context"
	"strings"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"golang.org/x/sync/errgroup"
	"spaziodati.eu/atoka-cli/common"
)

type testInput struct {
	path     string
	numValid int
	length   int
}

var _ = Describe("Input", func() {
	Describe("processing input", func() {
		var (
			ctx    context.Context
			g      *errgroup.Group
			input  testInput
			report chan common.ReportEntry
		)
		BeforeEach(func() {
			ctx = context.Background()
			g, ctx = errgroup.WithContext(ctx)
		})
		JustBeforeEach(func() {
			Expect(input.path).Should(BeARegularFile())
		})

		AssertExpectedOutput := func(input testInput) {
			report = make(chan common.ReportEntry, input.length)
			defer close(report)

			inputProcessor := NewPersonInputProcessor(input.path, common.ReportNotFound, true)
			output := inputProcessor.ProcessInput(ctx, g, report)

			for i := 0; i < input.numValid; i++ {
				Eventually(output).Should(Receive())
			}
			Eventually(report).Should(HaveLen(input.length))
			Eventually(output).Should(BeClosed())
		}

		Context("when parsing a valid txt", func() {
			BeforeEach(func() {
				input = testInput{GetTestFilePath("input_valid.txt"), 10, 10}
			})
			It("should output the expected number of items", func() {
				AssertExpectedOutput(input)
			})

			It("should ignore whitespace and empty lines", func() {
				input = testInput{GetTestFilePath("input_ignore_spaces.txt"), 10, 10}
				AssertExpectedOutput(input)
			})
		})

		Context("when parsing a valid xlsx", func() {
			BeforeEach(func() {
				input = testInput{GetTestFilePath("input_valid.xlsx"), 10, 10}
			})
			It("should output the expected number of items", func() {
				AssertExpectedOutput(input)
			})
		})

		Context("when parsing a txt with no valid ids", func() {
			BeforeEach(func() {
				input = testInput{GetTestFilePath("input_invalid.txt"), 0, 5}
			})
			It("should not output items", func() {
				AssertExpectedOutput(input)
			})
		})

		Context("when parsing a txt with duplicates", func() {
			BeforeEach(func() {
				input = testInput{GetTestFilePath("input_duplicate.txt"), 5, 10}
			})
			It("should not output duplicates", func() {
				AssertExpectedOutput(input)
			})
		})
	})

	Describe("chunk", func() {
		var (
			ctx context.Context
			g   *errgroup.Group
		)
		BeforeEach(func() {
			ctx, _ = context.WithCancel(context.Background())
			g, ctx = errgroup.WithContext(ctx)
		})
		It("should output chunks of the right size", func() {
			var (
				input = make(chan *common.CustomDataInputItem)
				size  = 10
				total = 95
			)
			g.Go(func() error {
				defer close(input)
				for i := 0; i < total; i++ {
					select {
					case input <- &common.CustomDataInputItem{"x", nil}:
					case <-ctx.Done():
						return ctx.Err()
					}
				}
				return nil
			})
			output := chunk(ctx, g, input, size)
			count := 0
			for c := range output {
				length := len(strings.Split(c, ","))
				Expect(length).Should(BeNumerically("<=", size))
				count += length
			}
			Expect(count).Should(BeEquivalentTo(total))
			Eventually(output).Should(BeClosed())
		})
	})
})
