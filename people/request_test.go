// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package people

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/onsi/gomega/ghttp"
	"golang.org/x/sync/errgroup"
	"spaziodati.eu/atoka-cli/common"
)

func parseResponse(path string) common.AAResponse {
	data, err := ioutil.ReadFile(path)


	if err != nil {
		panic(err)
	}
	var response common.AAResponse
	err = json.Unmarshal(data, &response)
	if err != nil {
		panic(err)
	}
	return response
}

func parseRequest(path string) string {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	return strings.Split(string(data), "\n")[0]
}

var _ = Describe("Request", func() {
	Describe("processing requests", func() {
		var (
			ctx            context.Context
			g              *errgroup.Group
			server         *ghttp.Server
			conf           AAConf
			args           common.DownloadArgs
			apiPageSize    string
			taxIds         chan string
			reportChan     chan common.ReportEntry
			requestResults <-chan common.AAItem

			token    = "test-token"
			packages = []string{"*"}
		)
		BeforeEach(func() {
			ctx = context.Background()
			g, ctx = errgroup.WithContext(ctx)
			server = ghttp.NewServer()

			conf = AAConf{
				common.AAConf{
					server.URL(),
					common.CustomDataUploadEndpoint,
					time.Second * 60,
					3,
					500 * time.Millisecond,
					4,
					10,
				},
				peopleEndpoint,
			}

			args = common.DownloadArgs{
				CommonArgs: common.CommonArgs{GlobalArgs: &common.GlobalArgs{Token: &token}},
				Packages:   &packages,
			}
			apiPageSize = "50"

			taxIds = make(chan string, 100)
			reportChan = make(chan common.ReportEntry, 100)
			requestResults = NewPeopleApiClient(conf).RequestMaster(ctx, g, args, taxIds, reportChan)
		})

		AfterEach(func() {
			Expect(reportChan).Should(BeClosed())
			Expect(requestResults).Should(BeClosed())
			Expect(taxIds).Should(BeClosed())
		})

		Context("With small request the client", func() {
			var (
				response common.AAResponse = parseResponse(GetTestFilePath("response_small.json"))
			)

			It("should be able to make requests to the peopleEndpoint", func() {
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", peopleEndpoint),
						ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
						ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
						ghttp.VerifyFormKV("limit", apiPageSize),
						ghttp.RespondWithJSONEncoded(http.StatusOK, response),
					),
				)

				taxIds <- parseRequest(GetTestFilePath("request_small.txt"))
				close(taxIds)

				Eventually(requestResults).Should(HaveLen(5))
				Eventually(reportChan).Should(HaveLen(5))

				Eventually(requestResults).Should(BeClosed())
				Eventually(reportChan).Should(BeClosed())
				Eventually(server.ReceivedRequests()).Should(HaveLen(1))
			})

			It("should be able retry when there are temporary errors", func() {
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", peopleEndpoint),
						ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
						ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
						ghttp.VerifyFormKV("limit", apiPageSize),
						ghttp.RespondWith(http.StatusGatewayTimeout, `{"message": "Gateway timeout"}`)),

					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", peopleEndpoint),
						ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
						ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
						ghttp.VerifyFormKV("limit", apiPageSize),
						ghttp.RespondWith(http.StatusTeapot, "42")),

					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", peopleEndpoint),
						ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
						ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
						ghttp.VerifyFormKV("limit", apiPageSize),
						ghttp.RespondWithJSONEncoded(http.StatusOK, response),
					),
				)

				taxIds <- parseRequest(GetTestFilePath("request_small.txt"))
				close(taxIds)

				var timeout = 3 * time.Second

				Eventually(requestResults, timeout).Should(HaveLen(5))
				Eventually(reportChan, timeout).Should(HaveLen(5))

				for range requestResults {
				}
				for range reportChan {
				}

				Eventually(requestResults, timeout).Should(BeClosed())
				Eventually(reportChan, timeout).Should(BeClosed())
				Eventually(server.ReceivedRequests(), timeout).Should(HaveLen(3))
			})
		})

		Context("With normal request the client", func() {
			var (
				response common.AAResponse = parseResponse(GetTestFilePath("response_normal.json"))
			)

			It("should be able to receive multiple requests", func() {
				var parts = 2
				for i := 0; i < parts; i++ {
					server.AppendHandlers(
						ghttp.CombineHandlers(
							ghttp.VerifyRequest("POST", peopleEndpoint),
							ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
							ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
							ghttp.RespondWithJSONEncoded(http.StatusOK, response),
						),
					)

					taxIds <- parseRequest(GetTestFilePath("request_normal.txt"))
				}

				close(taxIds)

				Eventually(requestResults).Should(HaveLen(48 * parts))
				Eventually(reportChan).Should(HaveLen(48 * parts))

				for range requestResults {
				}
				for range reportChan {
				}

				Eventually(requestResults).Should(BeClosed())
				Eventually(reportChan).Should(BeClosed())
				Eventually(server.ReceivedRequests()).Should(HaveLen(parts))
			})

			It("should be able to gracefully handle finishing credits associated with a token", func() {

				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", peopleEndpoint),
						ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
						ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
						ghttp.VerifyFormKV("limit", apiPageSize),
						ghttp.RespondWithJSONEncoded(http.StatusOK, response),
					),
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", peopleEndpoint),
						ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
						ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
						ghttp.VerifyFormKV("limit", apiPageSize),
						ghttp.RespondWith(http.StatusForbidden, `{"message": "Unauthorized token"}`),
					),
				)

				taxIds <- parseRequest(GetTestFilePath("request_normal.txt"))

				Eventually(requestResults).Should(HaveLen(48))
				Eventually(reportChan).Should(HaveLen(48))

				taxIds <- parseRequest(GetTestFilePath("request_normal.txt"))
				close(taxIds)

				for range requestResults {
				}
				for range reportChan {
				}

				Eventually(requestResults).Should(BeClosed())
				Eventually(reportChan).Should(BeClosed())
				Eventually(server.ReceivedRequests()).Should(HaveLen(2))
			})
		})

		Context("With a request that results in multiple items per tax id", func() {
			var (
				responsePage1 = parseResponse(GetTestFilePath("response_omocodia_1.json"))
				responsePage2 = parseResponse(GetTestFilePath("response_omocodia_2.json"))
			)

			It("should fetch all the results using pagination", func() {
				// Serve first page of results
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", peopleEndpoint),
						ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
						ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
						ghttp.VerifyFormKV("limit", apiPageSize),
						ghttp.RespondWithJSONEncoded(http.StatusOK, responsePage1),
					),
				)
				// Serve second page
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", peopleEndpoint),
						ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
						ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
						ghttp.VerifyFormKV("offset", "50"),
						ghttp.VerifyFormKV("limit", apiPageSize),
						ghttp.RespondWithJSONEncoded(http.StatusOK, responsePage2),
					),
				)

				taxIds <- parseRequest(GetTestFilePath("request_omocodia.txt"))

				Eventually(requestResults).Should(HaveLen(51))
				Eventually(reportChan).Should(HaveLen(51))

				close(taxIds)

				for range requestResults {
				}
				for range reportChan {
				}

				Eventually(requestResults).Should(BeClosed())
				Eventually(reportChan).Should(BeClosed())
				Eventually(server.ReceivedRequests()).Should(HaveLen(2))
			})
		})

		Context("With a request that results in multiple items per tax id", func() {
			var (
				responsePage1 = parseResponse(GetTestFilePath("response_omocodia_1.json"))
				responsePage2 = parseResponse(GetTestFilePath("response_omocodia_2.json"))
			)

			It("should fetch all the results using pagination", func() {
				// Serve first page of results
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", peopleEndpoint),
						ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
						ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
						ghttp.VerifyFormKV("limit", apiPageSize),
						ghttp.RespondWithJSONEncoded(http.StatusOK, responsePage1),
					),
				)
				// Serve second page
				server.AppendHandlers(
					ghttp.CombineHandlers(
						ghttp.VerifyRequest("POST", peopleEndpoint),
						ghttp.VerifyFormKV("token", *args.GlobalArgs.Token),
						ghttp.VerifyFormKV("packages", strings.Join(*args.Packages, ",")),
						ghttp.VerifyFormKV("offset", "50"),
						ghttp.VerifyFormKV("limit", apiPageSize),
						ghttp.RespondWithJSONEncoded(http.StatusOK, responsePage2),
					),
				)

				taxIds <- parseRequest(GetTestFilePath("request_omocodia.txt"))

				Eventually(requestResults).Should(HaveLen(51))
				Eventually(reportChan).Should(HaveLen(51))

				close(taxIds)

				for range requestResults {
				}
				for range reportChan {
				}

				Eventually(requestResults).Should(BeClosed())
				Eventually(reportChan).Should(BeClosed())
				Eventually(server.ReceivedRequests()).Should(HaveLen(2))
			})
		})

	})
})
