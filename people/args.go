// Copyright 2018 SpazioDati S.r.l.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package people

import (
	"fmt"

	"github.com/pkg/errors"
	"gopkg.in/alecthomas/kingpin.v2"
	"spaziodati.eu/atoka-cli/common"
)

func SetUpDownloadArgs(parentCmd *kingpin.CmdClause, globalArgs *common.GlobalArgs) (*kingpin.CmdClause, common.DownloadArgs) {
	var (
		args      common.DownloadArgs
		timestamp = common.GetTimeStamp()
	)

	peopleDownload := parentCmd.Command("download", "Download people data.")

	args.GlobalArgs = globalArgs

	args.Input = peopleDownload.
		Arg("input", "Path of the input file with the tax codes.").
		Required().
		ExistingFile()

	args.Packages = peopleDownload.
		Flag("packages", "Package to retrieve, can be specified multiple times").
		Default("*").
		Short('p').
		Action(func(ctx *kingpin.ParseContext) error {
			for _, p := range *args.Packages {
				if p == "*" || p == "base" {
					return nil
				}
			}
			return errors.New("You must include package base if you want to filter by taxcodes")
		}).
		Enums("*", "base", "atokaIndicators", "cervedIndicators", "companies", "shares", "realEstate", "socials")

	args.Output = peopleDownload.
		Flag("output", "File where to write the results.").
		Short('o').
		Default(fmt.Sprintf("output_%s.json", timestamp)).
		PlaceHolder("output_<NOW>.json").
		String()

	args.Report = peopleDownload.
		Flag("report", "File where to write the report.").
		Short('r').
		Default(fmt.Sprintf("report_%s.json", timestamp)).
		PlaceHolder("report_<NOW>.json").
		String()

	args.PageSize = peopleDownload.
		Flag("pagesize", "Paginate output in files of PAGESIZE elements. Setting it to 0 disables pagination").
		Short('P').
		Default("100000").
		Int()

	return peopleDownload, args
}

func SetUpUploadArgs(parentCmd *kingpin.CmdClause, globalArgs *common.GlobalArgs) (*kingpin.CmdClause, common.UploadArgs) {
	var (
		args      common.UploadArgs
		timestamp = common.GetTimeStamp()
	)
	peopleUpload := parentCmd.Command("upload", "Upload portfolio of people.")

	args.GlobalArgs = globalArgs

	args.Input = peopleUpload.
		Arg("input", "Path of the input file with the tax codes and data. Please use a tab separated csv file.").
		Required().
		ExistingFile()

	args.Portfolio = peopleUpload.
		Flag("customer-portfolio", "portfolio name").
		Short('c').
		Required().
		String()

	args.Report = peopleUpload.
		Flag("report", "File where to write the report.").
		Short('r').
		Default(fmt.Sprintf("report_%s.json", timestamp)).
		PlaceHolder("report_<NOW>.json").
		String()

	args.AppendMode = peopleUpload.
		Flag("append", "Append the data to the current version of the portfolio").
		Bool()

	return peopleUpload, args
}
